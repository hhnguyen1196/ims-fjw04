package com.fa.ims.controller;

public class BaseController {
    Integer getPageNumber(Integer page) {
        return page != null ? page : 0;
    }
}
